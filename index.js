var http = require('http');
var fs = require('fs');
var path = require('path');
var io = require('socket.io')(8080);

var Demo = require('./demo.js');
var server = http.createServer(function(req, res) {
    console.log(`${req.method} request for ${req.url}`);

    if (req.url === '/') {
        var ab = '{ "name":"John", "age":30, "city":"New York"}';
        var json = JSON.parse(ab);
        res.write(ab);
        res.end();
    } else if (true) {
        res.write("HTTP GET Request");
        res.end();
    } else if (type != undefined && type.match('folder') && realRequest.match('lineTraining')) {
        var result = ''
        for (var i = 1; i < 6; i++) {
            realRequest = `./img/ml/lineTraining/line${i}.png`;
            var img = fs.readFileSync(realRequest, { encoding: 'base64', flag: 'r' });
            if (i < 5)
                result += img + '-';
            else result += img;
        }

        res.writeHead(200, { 'Content-Type': 'image/png' });
        res.write(result)
        res.end();
        //res.end(img, 'base64');

    } else if (type != undefined && type.match('folder') && realRequest.match('Tier')) {
        var dirParent = './img/ml/Tier';
        var folder = [];
        var result = '';
        var check = 0;
        fs.readdir(dirParent, function(err, items) {
            items.forEach(element => {
                if (element.indexOf('.') < 0) {
                    folder.push(element)
                }
            });

            folder.forEach(element => {
                var subDir = dirParent + '/' + element;
                fs.readdir(subDir, function(err, items) {
                    items.forEach(element => {
                        var realDir = subDir + '/' + element;
                        var img = fs.readFileSync(realDir, { encoding: 'base64', flag: 'r' });
                        result += img + '-'
                        check++;
                    });
                    if (check == 25) {

                        res.writeHead(200, { 'Content-Type': 'image/png' });
                        res.write(result)
                        res.end();
                    }
                });
            });

        });


    } else {
        fs.readFile(realRequest, "UTF-8", function(err, html) {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.end(html);
        });
    }

    //show image in page
    //to display image
    if (req.url == "/img/logo.png") {
        console.log(`Request image in page ${req.url}`)
        var img = fs.readFileSync('./img/logo.png');
        res.writeHead(200, { 'Content-Type': 'image/png' });
        res.end(img, 'binary');

        return;

    }
    //for request favicon
    if (req.url.match("/requestFavicon" || req.url.match("/logo"))) {
        console.log('Request for favicon');

        var img = fs.readFileSync('img/favicon.png');
        res.writeHead(200, { 'Content-Type': 'image/x-icon' });
        res.end(img, 'binary');

        //var icoPath = path.join(__dirname, 'public', req.url);
        //var fileStream = fs.createReadStream(icoPath, "base64");
        //res.writeHead(200, {"Content-Type": "image/x-icon"});
        //fileStream.pipe(res);
    }
});
server.listen(8000, function() {
    console.log("Connect Success!!!")
});